class CategoriesController < ApplicationController

  before_action :set_category, only: [:show, :update, :destroy]
  
  def create
    category = Category.new(categories_params)
    if category.save
      render json: { message: category }, status: :created
    else
      render json: { errors: category.errors.messages }, status: 400
    end
  end

  def index
    category = Category.all
    render json: { category: category }, status: :ok 
  end

  def show
    render json: @category
  end

  def update
    @category.update( categories_params )
    render json: @category
  end

  def destroy
    render json: @category.delete
  end

  private
  def categories_params
    params.require(:category).permit(:name)
  end
  
  def set_category
    @category = Category.find_by(id: params[:id])
  end
  
end
