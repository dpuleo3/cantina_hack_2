class ClientsController < ApplicationController

  before_action :set_client, only: [:show, :update]

  def create
    client = Client.new(clients_params)
    if client.save
      render json: { message: client }, status: :created
    else
      render json: { errors: client.errors.messages }, status: 400
    end
  end

  def update
    @client.update( clients_params )
    render json: @client
  end

  def index
    client = Client.all
    render json: { client: client }, status: :ok 
  end

  def show
    render json: @client
  end

  private
  def clients_params
    params.require(:client).permit(:name, :ci, :email, :balance)
  end

  def set_clients
    @client = Client.find_by(id: params[:id])
  end

end
