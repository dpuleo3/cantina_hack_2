class InvoicesController < ApplicationController

  def create
    invoice = Invoice.new(invoices_params)
    if invoice.save
      render json: { message: invoice }, status: :created
    else
      render json: { errors: invoice.errors.messages }, status: 400
    end
  end

  def index
    @invoice = Invoice.all
    render json: { invoices: @invoices }, status: :ok 
  end

  def show
    render json: @invoice
  end
  
  private
  def invoices_params
    params.require(:invoice).permit(:number, :date, :price_per_unit, :quantity, :total)
  end
  
  def set_invoice
    @invoice = Invoice.find( params[:number, :date] )
  end 

end
