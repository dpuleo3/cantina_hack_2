class ProductsController < ApplicationController

  before_action :set_product, only: [:show, :update, :destroy]
  
  def create
    product = Product.new(products_params)
    if product.save
      render json: { message: product }, status: :created
    else
      render json: { errors: product.errors.messages }, status: 400
    end
  end

  def index
    product = Product.all
    render json: { product: product }, status: :ok 
  end

  def show
    render json: @product
  end

  def update
    @product.update( products_params )
    render json: @product
  end

  def destroy
    render json: @product.delete
  end

  private
  def products_params
    params.require(:product).permit(:name, :price, :description, :stock, :category_id)
  end
  
  def set_product
    @product = Product.find_by(id: params[:id])
  end

end


#que nutella vale 23?! ladron!!!!!!!!!!!!!!!!!!!!!!!!!!!!