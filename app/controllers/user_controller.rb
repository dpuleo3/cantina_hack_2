class UserController < ApplicationController

  def create
    user = User.new(user_params)
    if user.save
      render json: { message: user }, status: :created
    else
      render json: { errors: user.errors.messages }, status: 400
    end
  end

  def update
    @user.update( user_params )
    render json: @user
  end

  def index
    @user = User.all
    render json: { user: user }, status: :ok 
  end

  def show
    render json: @user
  end

  def destroy
    render json: @user.delete
  end

  private
  def user_params
    params.require(:user).permit(:kind, :name, :ci, :email, :password_digest)
  end
  
  def set_user
    @user = user.find( params[:name] )
  end

end
