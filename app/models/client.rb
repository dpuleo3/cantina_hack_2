class Client < ApplicationRecord

  has_many :sales

  validates :name, :email, :balance, :ci, presence: true
  validates :ci, numericality: true, uniqueness: true
  validates :email, uniqueness: true 
  validates :balance, numericality: true
  
  before_validation :normalize_name 

  private

  def normalize_name
    self.name = name.downcase
  end
  
end
