class Product < ApplicationRecord

  belongs_to :category
  has_and_belongs_to_many :sales 
    
  validates :name, :price, :description, :stock, presence: true
  validates :price, numericality: true

end

