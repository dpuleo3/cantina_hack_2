class Sale < ApplicationRecord

  belongs_to :client
  has_and_belongs_to_many :products
  
  validates :sale_number, :total, presence: true, numericality: true
  
end
