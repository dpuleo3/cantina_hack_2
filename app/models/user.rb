class User < ApplicationRecord

  validates :name, :ci, precence: true
  validates :ci, uniqueness: true, numericality: true
  validates :password_digest, presence: true, if: :client?

  def client?
    client == false
  end

end
