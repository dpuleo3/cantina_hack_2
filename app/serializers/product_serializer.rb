class ProductSerializer < ActiveModel::Serializer

  attributes :name, :price, :stock

end
