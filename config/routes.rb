Rails.application.routes.draw do

  get 'user/create'
  get 'user/update'
  get 'user/index'
  get 'user/show'
  get 'user/destroy'
  get 'client/create'
  get 'client/update'
  get 'client/index'
  get 'client/show'
  resources :products
  resources :details
  resources :clients
  resources :invoices
  resources :categories
  resources :invoices

end

 # get 'invoices/create'
  # get 'invoices/index'
  # get 'invoices/show'
  # get 'categories/create'
  # get 'categories/index'
  # get 'categories/show'
  # get 'categories/update'
  # get 'categories/destroy'
  # get 'products/create'
  # get 'products/index'
  # get 'products/show'
  # get 'products/update'
  # get 'products/destroy'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html