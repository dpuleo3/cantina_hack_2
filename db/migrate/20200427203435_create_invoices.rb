class CreateInvoices < ActiveRecord::Migration[6.0]
  def change
    create_table :invoices do |t|
      t.float :number, null: false 
      t.date :date 
      t.decimal :price_per_unit
      t.integer :quantity
      t.decimal :total

      t.references :client, foreign_key: true
      t.references :sale, foreign_key: true

      t.timestamps
    end
  end
end
