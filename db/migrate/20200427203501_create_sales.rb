class CreateSales < ActiveRecord::Migration[6.0]
  def change
    create_table :sales do |t|
      t.datetime :date
      t.integer :sale_number
      t.float :total
      
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
